<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index()
    {
        return $this->render('main.html.twig');
    }

    /**
     * @Route("/feedback",name="feedback", methods={"POST"})
     */
    public function feedback(Request $request, \Swift_Mailer $mailer){
        $email = $request->request->get('email') ?? null;
        $name = $request->request->get('name') ?? null;
        $message = $request->request->get('message') ?? null;
        $text_input = $request->request->get('text_input') ?? null;
        if(!$email || !$name || !$message || $text_input){
            $this->addFlash('error', 'Заполните все поля!');
        }
        else{
            //TODO:Перебить отправителя и получателя
            $text= (new \Swift_Message('New feedback'))
                ->setFrom('robot@ecomilk.ru')
                ->setTo('n.kovyzina@ecomilk.ru')
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'main/feedback.html.twig',
                        array('email' => $email,
                                'name' =>$name,
                                'message'=>$message)
                    ),
                    'text/html'
                );
            $mailer->send($text);
            $this->addFlash('success', 'Ваша заявка принята!');
        }
        return $this->redirectToRoute("main");
    }
}
