$(document).ready(function(){
    setTimeout(function(){
        $("#cube-loader").fadeOut();
    }, 1000);
    var col = $('#footer').css("background-color");
    var height_obj = $('#height_frame').css("height");
    $(window).scroll(function(){
        $('#main_nav').css({
            backgroundColor: col,
            opacity: ".9"
        });
        if($(window).width()>'768'){
            $('#video_frame').height(height_obj);
        }
        else{
            $('#video_frame').height(300);
        }
        onScrollHandler();
    });

    setInterval(function(){
        $('.scroll').animate({
            bottom: "6%"
        }, 500);
        $('.scroll').animate({
            bottom: "5%"
        }, 500);
    }, 1000);



    var pswpElement = document.querySelectorAll('.pswp')[0];

// build items array

    var items = [];
    setTimeout(function(){
        $.each($('.photo-slide'), function(a,b){
            var w = $(b).innerWidth();
            var h = $(b).innerHeight();
            var p = h/w;
            var wi = $(window).width()/1.5;
            var im = {
                src: $(b).attr("src"),
                w: wi,
                h: p*wi
            };
            items.push(im);
        });
    }, 1000);

    $('#show_more_photo').click(function(){
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    });

    $('.photo-slide').click(function(){
        var im = $(this).attr("src");
        var index = 0;
        var s_index = 0;
        $.each(items, function(a,b){
             if(b.src === im){
                 s_index = index;
             }
             index++;
        });
        var options = {
            index: s_index // start at first slide
        };
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    });

// define options (if needed)
    var options = {
        // optionName: 'option value'
        // for example:
        index: 0 // start at first slide
    };

// Initializes and opens PhotoSwipe
    $(".lazy").slick({dots: true});

    var position = $("#about").find("img").first().offset();
    $(".img_container").css({left: position.left-15});

});

$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: $($.attr(this, 'href')).offset().top-75}, 1000);
});

$(window).resize(function () {
    if($(window).width()>'768'){
        var height_obj = $('#height_frame').height();
        $('#video_frame').height(height_obj);
    }
    else{
        $('#video_frame').height(300);
    }
});

$('#callback_button').click(function (event) {
    event.preventDefault();
    $('html, body').animate({scrollTop: $('#callback').offset().top-75}, 1000);
});

$(".alex_flash").click(function () {
    $(this).fadeOut();
});

$(".buy_service").click(function () {
    var service_name = $(this).parents('.card').find("h3").html();
    $('#message').html("Вы выбрали услугу: "+service_name+"\n");
    $('html, body').animate({scrollTop: $("#callback").offset().top-75}, 1000);
    $("#name").focus();
});

$(".scroll").click(function () {
    $('html, body').animate({scrollTop: $(".page-text_1").offset().top-75}, 1000);
});

// Variables
const $window = $(window);
const $links = $('.menu li > a');
const $sections = getSections($links);
const $root = $('html, body');
const $hashLinks = $('a[href^="#"]:not([href="#"])');
// Events
$window.on('scroll', onScrollHandler);

// Body
activateLink($sections, $links);
// Functions
function getSections($links) {
    return $(
        $links.map(function(i, el){ return $(el).attr('href'); })
            .toArray()
            .filter(function(href){ return href.charAt(0) === '#' })
            .join(',')
    );
}

function activateLink($sections, $links) {
    const yPosition = $(window).scrollTop();

    for (j = $sections.length - 1; j >= 0; j -= 1) {
        const $section = $sections.eq(j);

        if (yPosition >= $section.offset().top - 75) {
            return $links
                .removeClass('active')
                .filter(`[href="#${$section.attr('id')}"]`)
                .addClass('active');
        }
    }
}

function onScrollHandler() {
    activateLink($sections, $links);
}




